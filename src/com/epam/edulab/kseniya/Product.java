package com.epam.edulab.kseniya;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"producer", "model", "color", "date", "price", "count"}, name = "product")
public class Product {
    @XmlAttribute
    private String name;
    private String producer;
    private String model;
    private String color;
    private Date date;
    private int price;
    private int count;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Name: ").append(name).append('\n');
        builder.append("Producer: ").append(producer).append('\n');
        builder.append("Model: ").append(model).append('\n');
        builder.append("Color: ").append(color).append('\n');
        builder.append("Date: ").append(date).append('\n');
        builder.append("Price: ").append(price).append('\n');
        builder.append("Count: ").append(count).append('\n');
        return builder.toString();
    }
}