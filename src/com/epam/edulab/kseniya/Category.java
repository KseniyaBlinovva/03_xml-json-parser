package com.epam.edulab.kseniya;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlType(name = "category")
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {
    @XmlAttribute
    private String name;
    @XmlElement(name = "subcategory")
    private List<SubCategory> subCategories = new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public String toString(){
        return "Sub category" + subCategories;
    }
}
