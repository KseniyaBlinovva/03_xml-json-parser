package com.epam.edulab.kseniya;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlType(name = "magazine")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement

public class Magazine {
    @XmlElement(name = "category")
    private List<Category> categories = new ArrayList<>();

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public String toString() {
        return "Category" + categories;
    }
}
