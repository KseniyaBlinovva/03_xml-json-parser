package com.epam.edulab.kseniya;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlType(name = "subcategory")
@XmlAccessorType(XmlAccessType.FIELD)

public class SubCategory {
    @XmlAttribute
    private String name;
    @XmlElement(name = "product")
    private List<Product> products = new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Subcategory{" + "product=" + products;

    }
}
